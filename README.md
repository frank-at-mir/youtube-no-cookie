# Youtube no-cookie

Simple Javascript to display a Youtube Preview with a Play Button but do not load anything from Youtube until Play is pressed.

Installation:

1.  Copy the files video-embed.css and video-embed.js to the Server.
1.  In the Header link to the CSS File e.g. <link rel="stylesheet" href="/video-embed/video-embed.css" type="text/css" />
1.  In the Footer link to the JS File e.g. <script src="/video-embed/video-embed.js"></script>

How to use:

1.  Download the preview Image from Youtube. You can use a Tool like this http://thumbnailsave.com/ or download it yourself from https://img.youtube.com/vi/[CODE]/maxresdefault.jpg
1.  Store the Image somewhere on your Server.
1.  Replace the original IFrame from Youttube with the youtube-no-cookie code:

   Orignal:
   
   `<iframe width="229" height="129" src="https://www.youtube-nocookie.com/embed/[CODE]?rel=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>`

   Replace with:
   
   `<div class="video-embed" data-id="[CODE]" data-image="video-image.jpg" data-autosize="1" data-width="229" data-height="129" style="min-height: 129px;"></div>`

   Make sure, the you replace the CODE with your own Videocode (e.g. A079J21UfLY).

Thats it, have fun.